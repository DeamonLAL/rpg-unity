﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mooving : MonoBehaviour
{
    public GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.W))
            player.transform.position = new Vector2(player.transform.position.x, 
                player.transform.position.y + 0.05f);
        if (Input.GetKey(KeyCode.S))
            player.transform.position = new Vector2(player.transform.position.x,
                player.transform.position.y - 0.05f);
        if (Input.GetKey(KeyCode.A))
            player.transform.position = new Vector2(player.transform.position.x - 0.05f,
                player.transform.position.y);
        if (Input.GetKey(KeyCode.D))
            player.transform.position = new Vector2(player.transform.position.x + 0.05f,
                player.transform.position.y);
    }
}
